#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
import json

NGW_ = dict(
    url='',
    creds=()
)


def patch_to_ngw(resource_id,feats):
    req = requests.patch(
        f'{NGW_["url"]}/api/resource/{resource_id}/feature/',
        data=json.dumps(feats),
        auth=NGW_['creds'],
        params={'dt_format':'iso'}
        )

    if req.status_code==200:
        req_json = req.json()
    else:
        return req.status_code,req.text
    return req.status_code,req_json


def del_feats_from_ngw(resource_id,feat_ids):
    req = requests.delete(
        f'{NGW_["url"]}/api/resource/{resource_id}/feature/',
        data=json.dumps(feat_ids),
        auth=NGW_['creds']
        )
    return req.status_code


def get_list_feats_from_id(resource_id,from_id):
    print(f'{NGW_["url"]}/api/resource/{resource_id}/feature/')
    req = requests.get(
        f'{NGW_["url"]}/api/resource/{resource_id}/feature/',
        params=dict(
            fields='REG',
            fld_id__ge=from_id,
            geom='no'
            ),
        auth=NGW_['creds']
        )
    print(req.request.path_url)
    if req.status_code==200:
        req_json = req.json()
    else:
        raise Exception("Req error", req.status_code)    
    return req_json


def clear_ngw_from_id(
    from_id,poly_id,line_id,nep_id=None):
        
    list_feats = get_list_feats_from_id(poly_id,from_id)
    if len(list_feats)>0:
        print_list = list(el['id'] for el in list_feats)
    else:
        print_list=list_feats
    print('list for del',print_list)
    print('del',del_feats_from_ngw(poly_id,list_feats))
    
    req = requests.get(
        f'{NGW_["url"]}/api/resource/{line_id}/feature/',
        params=dict(
            fields='plotid',
            fld_plotid__ge=from_id,
            geom='no'
            ),
        auth=NGW_['creds']
        )
    line_json = req.json()
    if len(line_json)>0:
        print_list = list(el['id'] for el in line_json)
    else:
        print_list=line_json
    print('list line for del', print_list)
    print('del',del_feats_from_ngw(line_id,line_json))
    if not(nep_id is None):
        req = requests.get(
            f'{NGW_["url"]}/api/resource/{nep_id}/feature/',
            params=dict(
                fields='plotid',
                geom='no'
                ),
            auth=NGW_['creds']
            )
        nep_json = req.json()
        # print('all nep', nep_json)
        nep_json=list(
            filter(lambda elem: int(elem['fields']['plotid'])>=from_id,nep_json))
        if len(nep_json)>0:
            print_list = list(el['id'] for el in nep_json)
        else:
            print_list=nep_json
        print('list nep for del', print_list)
        print('del',del_feats_from_ngw(nep_id,nep_json))
    
    return
