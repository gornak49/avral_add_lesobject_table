#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ogrutils

class LesObj():
    
    def __init__(self):
        self.pts=[]
        self.pg=[]
        
        self.bind_pts=[]
        
        self.neps_pts=[]
        self.neps=[]
        self.neps_pg={}


    def create_obj_pg(self):
        if self.pts!=[]:
            self.pts.sort(key=lambda p:p['pt_id'])
            points=list(p['geom'] for p in self.pts)
            points.append(self.pts[0]['geom'])
            pg_geom = ogrutils.create_feat(points,'wkbPolygon')
            self.pg.append(dict(
                geom=pg_geom,
                geom_type='poly',
                parent_id=self.id,
                type='object',
                name='границы отвода'
                ))
            
            
    def create_nep_pg(self):
        self.load_nep_from_pt()
        if self.neps!=[]:
            for nep in self.neps:
                points_=list(
                    filter(
                        lambda p:p['type']=='nep' \
                            and p['geom_type']=='point' \
                            and p['pt_type']==1 \
                            and p['nep_id']==nep,
                        self.neps_pts
                        )
                    )
                points_.sort(key=lambda p:p['pt_id'])
                points=list(p['geom'] for p in points_)
                points.append(points[0])
                pg_geom = ogrutils.create_feat(points,'wkbPolygon')
                self.neps_pg[nep]=(dict(
                    geom=pg_geom,
                    geom_type='poly',
                    parent_id=self.id,
                    type='nep',
                    name='нэп'
                    ))
            
            
    def load_nep_from_pt(self):
        if self.neps_pts!=[]:
            self.neps = list(set(list(p['nep_id'] for p in self.neps_pts)))
            
            
    def create_ogr_pts_geom(self):
        if len(self.bind_pts)>0:
            for p in self.bind_pts:
                p['geom_']=ogrutils.create_feat([p['geom']],'wkbPoint')
        
