#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

from avral import avral
from avral.operation import AvralOperation, OperationException
from avral.io.types import *
from avral.io.responce import AvralResponce

class AddLesobjectTable(AvralOperation):
    def __init__(self):
        super(AddLesobjectTable, self).__init__(
            name="add_lesobject_table",
            inputs=[
                ("xlsx", FileType()),
                ("ngw_url", StringType()),
                ("ngw_login", StringType()),
                ("ngw_pas", StringType()),
                ("crs", StringType()),
                ("ngw_les_id", StringType()),
                ("ngw_les_line_id", StringType()),
                ("ngw_nep_id", StringType()),
                ("ngw_infra_id", StringType()),
                ("ngw_infra_line_id", StringType()),
                #check
                ("del_from_fid", StringType())
                #avral_start
            ],
            outputs=[
                (u"report",StringType())
            ],
        )
        
    def _do_work(self):
        
        for arg in self.inputs:
            arg_val = self.getInput(arg)
            print(arg,type(arg_val),arg_val)
        if not(os.path.isfile(self.getInput("xlsx"))):
            raise OperationException("не указан файл xlsx")
        if self.getInput("ngw_url")=='':
            raise OperationException("не указан url")
        if self.getInput("ngw_login")=='':
            raise OperationException("не указан login")
        if self.getInput("ngw_pas")=='':
            raise OperationException("не указан пароль")
        
        if self.getInput("crs"):
            crs = self.getInput("crs")
        else:
            crs = '4326'
        
        if self.getInput("ngw_les_id"):
            ngw_les_id = self.getInput("ngw_les_id")
        else:
            ngw_les_id = "''"
        if self.getInput("ngw_infra_id"):
            ngw_infra_id = self.getInput("ngw_infra_id")
        else:
            ngw_infra_id = "''"
        if self.getInput("ngw_les_line_id"):
            ngw_les_line_id = self.getInput("ngw_les_line_id")
        else:
            ngw_les_line_id = "''"
        if self.getInput("ngw_infra_line_id"):
            ngw_infra_line_id = self.getInput("ngw_infra_line_id")
        else:
            ngw_infra_line_id = "''"
        if self.getInput("ngw_nep_id"):
            ngw_nep_id = self.getInput("ngw_nep_id")
        else:
            ngw_nep_id = "''"
            
        if self.getInput("del_from_fid"):
            del_from_fid = self.getInput("del_from_fid")
        else:
            del_from_fid = "''"
            
        cmd = "python " + os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            "add_lesobject_table.py"
            )
            
        cmd = cmd + " " + " ".join((
            "--xlsx", "'{xlsx}'",
            "--ngw_url", "'{ngw_url}'",
            "--ngw_login", "'{ngw_login}'",
            "--ngw_pas", "'{ngw_pas}'",
            "--crs","'{crs}'",
            "--ngw_les_id", "'{ngw_les_id}'",
            "--ngw_les_line_id", "'{ngw_les_line_id}'",
            "--ngw_nep_id", "'{ngw_nep_id}'",
            "--ngw_infra_id", "'{ngw_infra_id}'",
            "--ngw_infra_line_id", "'{ngw_infra_line_id}'",
            "--del_from_fid", "'{del_from_fid}'",
            "--check", '1',
            "--avral_start", '1'
            ))
            
        cmd = cmd.format(
            xlsx=self.getInput("xlsx"),
            ngw_url=self.getInput("ngw_url"),
            ngw_login= self.getInput("ngw_login"),
            ngw_pas= self.getInput("ngw_pas"),
            crs=crs,
            ngw_les_id=ngw_les_id,
            ngw_les_line_id=ngw_les_line_id,
            ngw_nep_id=ngw_nep_id,
            ngw_infra_id=ngw_infra_id,
            ngw_infra_line_id=ngw_infra_line_id,
            del_from_fid=del_from_fid
            )
            
        print('-'*30)
        print(cmd)
        with open(os.path.join('../','report.txt'), "w",encoding="utf-8") as f:
            f.write('')
        os.system(cmd)
        with open('../report.txt',encoding='utf-8') as f:
            report_text = f.read()
        if 'Error:' in report_text:
            raise OperationException(report_text.split('Error: ')[1])
        self.setOutput(u"report", report_text)
        print('-'*30)
        print('end _do_work')
        
        return ()
