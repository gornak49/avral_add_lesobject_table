#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import zipfile
from io import BytesIO, TextIOWrapper
import sys
import shutil
from pprint import pprint


def log(*args,q=False,s=0,e=0,pp=False,sep=' ',ch='-'):
    if q:
        return
    else:
        print((ch*s+'\n') if s>0 else '',sep='',end='')
        if pp:
            pprint(*args)
        else:
            print(*args,sep=sep)
        print((ch*e+'\n') if e>0 else '',sep='',end='')
    return


def error(messages):
    for msg in messages:
        print(msg)
    exit()
    return


def create_dir(cwd,dirName,replace=False):
    path = os.path.join(cwd,dirName)
    if os.path.exists(path) and\
        replace==True:
        shutil.rmtree(path)
    os.makedirs(path,exist_ok=True)
    return path


def get_zff_names_objs(input_f_exts,binary,
    exist_zff_names_objs,path_zip):

    with zipfile.ZipFile(binary) as z:
        try:
            encode_names = list(
                name.encode('cp437').decode('cp866') for name in z.namelist()
            )
            print(z.filename,'code 437/866')
        except UnicodeError:
            encode_names = list(
                name for name in z.namelist()
            )
            print(z.filename,'code utf-8')
        # # zf_name - file name with interior path in zip
        filter_zf_names = list(
            filter(
                lambda zf_name: os.path.splitext(zf_name)[1] in input_f_exts,
                encode_names
                )
            )
        if len(filter_zf_names):
            # print('-'*10)
            # print("z.filename",path_zip,z.filename,type(z))

            exist_zff_names_objs.extend(
                list(
                        (
                            path_zip,
                            *os.path.splitext(zf_name)
                            )
                        for zf_name in filter_zf_names
                        )
                )
        
        interior_zips = list(
            filter(
                lambda zf_name: '.zip' in zf_name,
                encode_names
                )
            )
        if len(interior_zips):
            for zf_name in interior_zips:
                with BytesIO(z.read(zf_name)) as zf_data:
                    exist_zff_names_objs = get_zff_names_objs(
                        input_f_exts,
                        binary=zf_data,
                        exist_zff_names_objs=exist_zff_names_objs,
                        path_zip=[*path_zip,zf_name])
    return exist_zff_names_objs


def get_ff_names_objs(input_path,input_f_exts):

    ff_names_objs = []
    if os.path.isfile(input_path):
        root, f = os.path.split(input_path)
        f_name, f_ext = os.path.splitext(f)
        if f_ext in input_f_exts:
            ff_names_objs.append([root,f_name,f_ext])
            return ff_names_objs
        else:
            return []
    elif os.path.isdir(input_path):
        for root,dirs,files in os.walk(input_path):
            f_names_objs = list(
                os.path.splitext(f) for f in files
                )
            filter_f_names_objs = list(
                filter(
                    lambda f_name_obj: f_name_obj[1] in input_f_exts,
                    f_names_objs
                    )
                )
            if len(filter_f_names_objs):
                ff_names_objs.extend(
                    list(
                        (root,f_name_obj[0],f_name_obj[1])
                        for f_name_obj in filter_f_names_objs
                        )
                    )
    return ff_names_objs


def get_aff_names_objs(input_path,input_f_exts):
    zff_names_objs = []
    ff_names_objs = get_ff_names_objs(input_path,input_f_exts)
    root_zips = get_ff_names_objs(input_path,['.zip'])
    if len(root_zips):
        for root,f_name,f_ext in root_zips:
            full_zipname = os.path.join(root,f_name+f_ext)
            with open(full_zipname,'rb') as zf:
                binary = zf
                current_zff_names_objs = get_zff_names_objs(
                    input_f_exts,
                    binary,
                    exist_zff_names_objs = [],
                    path_zip=[full_zipname]
                    )
            if len(current_zff_names_objs):
                zff_names_objs.extend(current_zff_names_objs)
    return ff_names_objs,zff_names_objs


def read_interior_zip(parent_zip,zf_name):
    # print(parent_zip,zf_name)
    binary  = BytesIO(parent_zip.read(zf_name))
    zip_data = zipfile.ZipFile(binary)
    # print(zip_data)
    return zip_data


def read_afile(aff_name, aff_name_type='string'):
    data=None
    ff_name = ''
    if aff_name_type=='object':
        aff_name_obj_type=str(type(aff_name[0]))
        if aff_name_obj_type=="<class 'str'>":
            root, f_name, f_ext = aff_name
            ff_name = os.path.join(root, f_name + f_ext)
            if f_ext == '.zip':
                print('!zip')
                raise TypeError
            else:
                with open(ff_name,encoding='utf-8') as f:
                    data = f.read()
        elif aff_name_obj_type=="<class 'list'>":
            zroot = aff_name[0][0]
            ff_name = zroot
            with zipfile.ZipFile(zroot) as z:
                interior_zf_data = z
                for interior_zf_name in aff_name[0][1:]:
                    ff_name=ff_name + '/' + interior_zf_name
                    interior_zf_data = read_interior_zip(
                        interior_zf_data,
                        interior_zf_name
                        )
                newname = aff_name[1] + aff_name[2]
                if newname in interior_zf_data.namelist():
                    pass
                else:
                    newname = newname.encode('cp866').decode('cp437')
                with interior_zf_data as izf:
                    with TextIOWrapper(
                        izf.open(newname), encoding="utf-8"
                        ) as interior_f:
                        data = interior_f.read()
                ff_name = ff_name + aff_name[1]+ aff_name[2]
        else:
            raise TypeError
    elif aff_name_type=='string':
        ff_name = aff_name
        with open(ff_name,encoding='utf-8') as f:
            data = f.read()
    return data, ff_name

    
def add_ext(filename):
    filename_added_ext=filename
    f_name, f_ext = os.path.splitext(filename)
    if not(f_ext):
        if zipfile.is_zipfile(filename):
            ext = '.zip'
        else:
            ext = '.xml'
        filename_added_ext = filename+ext
        os.rename(filename,filename+ext)    
    return filename_added_ext
