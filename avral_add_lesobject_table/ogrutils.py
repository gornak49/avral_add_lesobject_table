#!/usr/bin/env python
# -*- coding: utf-8 -*-

from osgeo import ogr
from osgeo import osr
import json
import sqlite3

GTYPES={
    'wkbLineString': ogr.wkbLineString,
    'wkbPolygon': ogr.wkbPolygon,
    'wkbPoint': ogr.wkbPoint
    }
    

DNAMES={
    'geojson': 'GeoJSON',
    'shp': 'ESRI Shapefile',
    'gpkg': 'GPKG',
    'tab': 'MapInfo File'
    }

FTYPES={
    'string': ogr.OFTString,
    'real': ogr.OFTReal,
    'date': ogr.OFTDate
    }
    
    
def create_feat(points,geom_type):
    wkbGeomType = GTYPES[geom_type]
    base_geom = ogr.Geometry(wkbGeomType)
    if geom_type == 'wkbLineString':
        for point in points:
            base_geom.AddPoint(point[0],point[1])
    elif geom_type == 'wkbPolygon':
        ring = ogr.Geometry(ogr.wkbLinearRing)
        for point in points:
            ring.AddPoint(point[0],point[1])
        base_geom.AddGeometry(ring)
    elif geom_type=='wkbPoint':
        for point in points:
            base_geom.AddPoint(point[0],point[1])
    base_geom.FlattenTo2D()
    return base_geom


def view_feat(geom,outformat='json'):
    geom = geom.ExportToJson()
    feat = json.loads(feat)
    return feat


def create_mem_ds(feats,layer_name,geomkey,geom_type,srs_epsg=4326,field_dict=None):
    out_driver = ogr.GetDriverByName('MEMORY')
    data_source = out_driver.CreateDataSource('memData')
    # tmp = out_driver.Open('memData',1)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(srs_epsg)
    layer = data_source.CreateLayer(layer_name, srs, GTYPES[geom_type])
    if field_dict:
        for key,value in field_dict.items():
            field_def = ogr.FieldDefn(key,FTYPES[value])
            layer.CreateField(field_def)
    else:
        for key in feats[0]['attrs']:
            field_def = ogr.FieldDefn(key)#, ogr.OFTString)
            layer.CreateField(field_def)
    for feat in feats:
        points = feat['geoms'][geomkey]
        if len(points):
            feature = ogr.Feature(layer.GetLayerDefn())
            geom = create_feat(points,geom_type)
            feature.SetGeometry(geom)
            for key,value in feat['attrs'].items():
                if field_dict:
                    if key in field_dict:
                        feature.SetField(key,value)       
                else:
                    feature.SetField(key,value)
            layer.CreateFeature(feature)
            feature = None
    return data_source
    

def export_ds(input_ds,layername,out_dest,outformat):
    driver_name = DNAMES[outformat]
    outdriver=ogr.GetDriverByName(driver_name)
    out_data_source = outdriver.CreateDataSource(out_dest)
    out_layer=out_data_source.CopyLayer(input_ds.GetLayer(layername),layername,['ENCODING=utf8'])
    # out_data_source = None
    return out_data_source


def transform(geom,in_crs,out_crs,db_path):
    in_crs_=get_crs_from_string(in_crs,db_path)
    out_crs_=get_crs_from_string(out_crs,db_path)
    if in_crs_ is None or out_crs_ is None:
        return None
    transform = osr.CoordinateTransformation(in_crs_, out_crs_)
    transform_er=geom.Transform(transform)
    if transform_er!=0:
        return None
    return geom

def get_crs_from_db_by_name(crs_name,db_path):
    proj_par=None
    con = sqlite3.connect(db_path)
    cur = con.cursor()
    qery_pattern='''
        SELECT
            "parameters"
        FROM
            "tbl_srs"
        WHERE
            "description"='{crs_name}'
        '''
    check_query = qery_pattern.format(
        crs_name=crs_name)
    cur.execute(check_query)
    res=cur.fetchone()
    if not(res is None):
        proj_par=res[0]
    else:
        crs_name_=crs_name.split(" ")
        if len(crs_name_)==4:
            qery_pattern='''
                SELECT
                    "parameters"
                FROM
                    "tbl_srs"
                WHERE
                    "description" LIKE '%МСК%{crs_name_[1]}%зона%{crs_name_[3]}%'
                '''
            check_query = qery_pattern.format(
                crs_name_=crs_name_)
            cur.execute(check_query)
            res=cur.fetchone()
    if not(res is None):
        proj_par=res[0]
    con.close()
    return proj_par


def get_crs_from_string(input_crs,db_path):
    input_crs=" ".join(input_crs.split())
    len_input=len(input_crs)
    crs=osr.SpatialReference()
    if input_crs[0].isdigit():
        crs_er=crs.ImportFromEPSG(int(input_crs))
    elif input_crs[0:3].upper()=='МСК':
        proj_par=get_crs_from_db_by_name(input_crs,db_path)
        if proj_par is None:
            crs_er=1
        else:
            crs_er=crs.ImportFromProj4(proj_par)
    elif input_crs[0:5]=='+proj':
        crs_er=crs.ImportFromProj4(input_crs)
    elif input_crs[0:6]=='PROJCS' or input_crs[0:6]=='GEOGCS':
        crs_er=crs.ImportFromWkt(input_crs)
    else:
        crs_er=1
    if crs_er!=0:
        return None
    else:
        pass
        crs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    return crs
