#!/usr/bin/env python
# -*- coding: utf-8 -*-

POLY_TYPES = {
    'les_id': 'les',
    'infra_id': 'infra',
    }

NEP_ID_FIELD = 'nep_id'
ID_KEY_FIELD = 'plotid'

GEOM_SHEET_NAMES = {
    'les': 'Координаты лесосеки',
    'infra': 'Координаты инфраструктуры',
    'nep': 'Координаты НЭП'
    }

GEOM_QUERY = {
    'les': '''
        SELECT
            "_rowid_",
            "_rowid_" as "pt_id",
            CAST("Field1" as integer) as "parent_id",
            CAST("Field2" as string) as "pt_type",
            CAST("Field3" as string) as "pt_name",
            "Field4" as "lat",
            "Field5" as "lon"
        FROM
            "{lyr_name}"
        WHERE
            "parent_id" > 0
        ORDER BY "_rowid_"
        ''',
    'infra': '''
        SELECT
            "_rowid_",
            "_rowid_" as "pt_id",
            CAST("Field1" as integer) as "parent_id",
            CAST("Field2" as string) as "pt_type",
            CAST("Field3" as string) as "pt_name",
            "Field4" as "lat",
            "Field5" as "lon"
        FROM
            "{lyr_name}"
        WHERE
            "parent_id" > 0
        ORDER BY "_rowid_"
        ''',
    'nep': '''
        SELECT
            "_rowid_",
            "_rowid_" as "pt_id",
            CAST("Field1" as integer) as "parent_id",
            CAST("Field2" as integer) as "nep_id",
            CAST("Field3" as string) as "pt_type",
            CAST("Field4" as string) as "pt_name",
            "Field5" as "lat",
            "Field6" as "lon"
        FROM
            "{lyr_name}"
        WHERE
            "parent_id" > 0
        ORDER BY "_rowid_"
        '''
    }

SHEET_NAMES = {
    'les': 'Атрибуты лесосеки',
    'infra': 'Атрибуты инфраструктуры',
    'nep': 'Атрибуты НЭП'
    }
    
FIELDS={
    'nep': {
        'les_id': 'Field1',
        'nep_id': 'Field2',
        'NEP_NAME': 'Field3',
        'NEP_AREA': 'Field4'
        },

    'infra': {
        'id': 'Field1',
        'INF_NOM': 'Field2',
        'DESC': 'Field3',
        'INF_ACTION': 'Field4',
        'REG': 'Field5',
        'MUN': 'Field6',
        'LES': 'Field7',
        'UCH_LES': 'Field8',
        'UROCH': 'Field9',
        'KV': 'Field10',
        'vd': 'Field11',
        'area': 'Field12',
        'FORMA_RUB': 'Field13',
        'TYPE_RUB': 'Field14',
        'AREA_PLAN': 'Field15',
        'YEAR': 'Field16',
        'TYPE_MER': 'Field17',
        'poroda': 'Field18',
        'farm': 'Field19',
        'formaRub': 'Field20',
        'typeRub': 'Field21',
        'volume': 'Field22'
        },

    'les': {
        'id': 'Field1',
        'NOM_LESKEY': 'Field2',
        'REG': 'Field3',
        'MUN': 'Field4',
        'LES': 'Field5',
        'UCH_LES': 'Field6',
        'UROCH': 'Field7',
        'KV': 'Field8',
        'vd': 'Field9',
        'area': 'Field10',
        'FORMA_RUB': 'Field11',
        'TYPE_RUB': 'Field12',
        'AREA_PLAN': 'Field13',
        'UNITS': 'Field14',
        'YEAR_DEV': 'Field15',
        'TYPE_MER': 'Field16',
        'poroda': 'Field17',
        'farm': 'Field18',
        'formaRub': 'Field19',
        'typeRub': 'Field20',
        'volume': 'Field21'
        }
    }
    
FIELDS_COMMONS={
    'all':{
        'REG': 'subject',
        # 'MUN': 'municipalDistrict',
        'LES': 'forestry',
        'UCH_LES': 'subforestry',
        # 'UROCH': 'tract',
        'TYPE_RUB': 'typeCutting',
        'typeRub': 'typeCutting',
        'poroda': 'tree',
        'FORMA_RUB': 'formCutting',
        'formaRub': 'formCutting',
        'farm': 'farm',
        'TYPE_MER': 'type_mer'
        },
    'les':{
        'UNITS': 'unitType'
        },
    'infra':{
        'DESC':  'object',
        'INF_ACTION': 'measure'
        }
    }

QUERY = {
    'les': '''
        SELECT
            "ID",
            "NOM_LESKEY",
            "REG",
            "MUN",
            "LES",
            "UCH_LES",
            "UROCH",
            "KV",
            "FORMA_RUB",
            "TYPE_RUB",
            "AREA_PLAN",
            "UNITS",
            "YEAR_DEV",
            "TYPE_MER",
            "v"."vd" as "__vd",
            "v"."area" as "__area",
            group_concat("_vd",";") as "_vd",
            group_concat("_poroda",";") as "_poroda",
            group_concat("_farm",";") as "_farm",
            group_concat("_formaRub",";") as "_formaRub",
            group_concat("_typeRub",";") as "_typeRub",
            group_concat("_volume",";") as "_volume"
        FROM(
            SELECT
                {fields[id]} as "ID",
                {fields[NOM_LESKEY]} as "NOM_LESKEY",
                {fields[REG]} as "REG",
                {fields[MUN]} as "MUN",
                {fields[LES]} as "LES",
                {fields[UCH_LES]} as "UCH_LES",
                {fields[UROCH]} as "UROCH",
                {fields[KV]} as "KV",
                {fields[FORMA_RUB]} as "FORMA_RUB",
                {fields[TYPE_RUB]} as "TYPE_RUB",
                {fields[AREA_PLAN]} as "AREA_PLAN",
                {fields[UNITS]} as "UNITS",
                {fields[YEAR_DEV]} as "YEAR_DEV",
                {fields[TYPE_MER]} as "TYPE_MER",
                {fields[vd]} as "_vd",
                {fields[poroda]} as "_poroda",
                {fields[farm]} as "_farm",
                {fields[formaRub]} as "_formaRub",
                {fields[typeRub]} as "_typeRub",
                {fields[volume]} as "_volume"
            FROM
                "{lyr_name}"
            WHERE
                CAST("ID" AS integer) > 0
            )
            LEFT JOIN(
                SELECT
                    "ID",
                    group_concat("vd",";") as "vd",
                    group_concat("area",";") as area
                FROM(
                    SELECT
                        {fields[id]} as "ID",
                        {fields[vd]} as "vd",
                        {fields[area]} as "area"
                    FROM
                        "{lyr_name}"
                    WHERE
                        CAST("ID" AS integer) > 0
                    GROUP BY
                        "ID", "vd"
                    )
                GROUP BY "ID"
                )
                AS "v"
            USING ("ID")
        GROUP BY "ID"
        ORDER BY CAST("ID" AS integer)
        ''',
    'infra': '''
        SELECT
            "ID",
            "INF_NOM",
            "DESC",
            "INF_ACTION",
            "REG",
            "MUN",
            "LES",
            "UCH_LES",
            "UROCH",
            "KV",
            "FORMA_RUB",
            "TYPE_RUB",
            "AREA_PLAN",
            "YEAR",
            "TYPE_MER",
            "v"."vd" as "__vd",
            "v"."area" as "__area",
            group_concat("_vd",";") as "_vd",
            group_concat("_poroda",";") as "_poroda",
            group_concat("_farm",";") as "_farm",
            group_concat("_formaRub",";") as "_formaRub",
            group_concat("_typeRub",";") as "_typeRub",
            group_concat("_volume",";") as "_volume"
        FROM(
            SELECT
                {fields[id]} as "ID",
                {fields[INF_NOM]} as "INF_NOM",
                {fields[DESC]} as "DESC",
                {fields[INF_ACTION]} as "INF_ACTION",
                {fields[REG]} as "REG",
                {fields[MUN]} as "MUN",
                {fields[LES]} as "LES",
                {fields[UCH_LES]} as "UCH_LES",
                {fields[UROCH]} as "UROCH",
                {fields[KV]} as "KV",
                {fields[FORMA_RUB]} as "FORMA_RUB",
                {fields[TYPE_RUB]} as "TYPE_RUB",
                {fields[AREA_PLAN]} as "AREA_PLAN",
                {fields[YEAR]} as "YEAR",
                {fields[TYPE_MER]} as "TYPE_MER",
                {fields[vd]} as "_vd",
                {fields[poroda]} as "_poroda",
                {fields[farm]} as "_farm",
                {fields[formaRub]} as "_formaRub",
                {fields[typeRub]} as "_typeRub",
                {fields[volume]} as "_volume"
            FROM
                "{lyr_name}"
            WHERE
                CAST("ID" AS integer) > 0
            )
            LEFT JOIN(
                SELECT
                    "ID",
                    group_concat("vd",";") as "vd",
                    group_concat("area",";") as area
                FROM(
                    SELECT
                        {fields[id]} as "ID",
                        {fields[vd]} as "vd",
                        {fields[area]} as "area"
                    FROM
                        "{lyr_name}"
                    WHERE
                        CAST("ID" AS integer) > 0
                    GROUP BY
                        "ID", "vd"
                    )
                GROUP BY "ID"
                )
                AS "v"
            USING ("ID")
        GROUP BY "ID"
        ORDER BY CAST("ID" AS integer)
        ''',
    'nep': '''
        SELECT
            {fields[les_id]} as "plotid",
            {fields[nep_id]} as "ID",
            {fields[NEP_NAME]} as "NEP_NAME",
            {fields[NEP_AREA]} as "NEP_AREA"
        FROM
            "{lyr_name}"
        WHERE
            CAST("ID" AS integer) > 0
        ORDER BY CAST("ID" AS integer)
        '''
    }

custom_catalog={
    "formCutting": {
        "names": [
            'Сплошная рубка',
            'Выборочная рубка'
            ],
        "abbr": []
        },

    "farm": {
        "names": [
            'Хвойное',
            'Мягколиственное',
            'Твердолиственное'
            ],
        "abbr": []
        },

    "type_mer": {
        "names": [
            'Естественное лесовосстановление',
            'Искусственное лесовосстановление',
            'Комбинированное лесовосстановление'
            ],
        "abbr": []
        }
    }

SERVICE_FIELDS={
    'serviceUser':{'name':'serviceUser'},
    'serviceCreatedAt':{'name':'serviceCreatedAt'},
    'serviceUpdatedAt':{'name':'serviceUpdatedAt'}
    }
