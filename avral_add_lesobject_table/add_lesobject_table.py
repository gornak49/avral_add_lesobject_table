#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import zipfile
import requests
import json

from osgeo import gdal
# from osgeo import ogr
# from osgeo import osr

from pprint import pprint
from datetime import datetime

import fileutils
import rlh_check
import ngw_utils

from fileutils import log
from lsgeom import LesObj
from ogrutils import transform
from const import POLY_TYPES,SHEET_NAMES,FIELDS,FIELDS_COMMONS,QUERY
from const import custom_catalog
from const import NEP_ID_FIELD,ID_KEY_FIELD,SERVICE_FIELDS
from const import GEOM_SHEET_NAMES,GEOM_QUERY

def get_args():
    p = argparse.ArgumentParser(description='add_lesobject_table')
    
    p.add_argument(
        '--xlsx', type=str,
        help='fullfilename xlsx file with attr',
        required=True
        )
    p.add_argument(
        '--ngw_url', type=str,
        help='web gis url',
        required=True
        )
    p.add_argument(
        '--ngw_login', type=str,
        help='login',
        required=True
        )
    p.add_argument(
        '--ngw_pas', type=str,
        help='passworld',
        required=True
        )
    p.add_argument(
        '--crs', type=str,
        help='input crs',
        default='4326',
        required=False
        )
    p.add_argument(
        '--ngw_les_id', type=str,
        help='ngw lesoseki resourse update id',
        required=False
        )
    p.add_argument(
        '--ngw_les_line_id', type=str,
        help='ngw resourse update id',
        required=False
        )
    p.add_argument(
        '--ngw_nep_id', type=str,
        help='ngw resourse update id',
        required=False
        )
    p.add_argument(
        '--ngw_infra_id', type=str,
        help='ngw infra update id',
        required=False
        )
    p.add_argument(
        '--ngw_infra_line_id', type=str,
        help='ngw resourse update id',
        required=False
        )
    p.add_argument(
        '--check', type=bool,
        help='check from catalog',
        default=True,
        required=False
        )
    p.add_argument(
        '--del_from_fid', type=str,
        help='delete feats',
        default=None,
        required=False
        )
    p.add_argument(
        '--avral_start', type=bool,
        help='start in avral',
        default=False,
        required=False
        )
    args = p.parse_args()

    if args.ngw_les_id=='':
        args.ngw_les_id=None
    if args.ngw_infra_id=='':
        args.ngw_infra_id=None
    if args.ngw_les_line_id=='':
        args.ngw_les_line_id=None
    if args.ngw_infra_line_id=='':
        args.ngw_infra_line_id=None
    if args.ngw_nep_id=='':
        args.ngw_nep_id=None
    if args.del_from_fid=='':
        args.del_from_fid=None

    return args


def get_attrs(xlsx_path,query,get_fields=False):
    attrs = {}
    fields={}
    data_set = gdal.OpenEx(xlsx_path)
    lyr= data_set.ExecuteSQL(query,dialect='SQLite')
    if not(lyr is None):
        ldefn = lyr.GetLayerDefn()
        schema = [ldefn.GetFieldDefn(n).name for n in range(ldefn.GetFieldCount())]
        if get_fields:
            for field in schema:
                fields[field]=[]
        log(lyr,q=True)
        log(lyr.GetName(),' :',lyr.GetFeatureCount(),q=True)
        for feature in lyr:
            json_feature = feature.ExportToJson(as_object=True)['properties']
            try:
                attrs[int(json_feature['ID'])]=json_feature
                attrs[int(json_feature['ID'])].pop('ID')
            except KeyError:
                attrs[int(json_feature['rowid'])]=json_feature
            if get_fields:
                for field in schema:
                    fields[field].append(feature[field])
    data_set.ReleaseResultSet(lyr) 
    data_set = None
    return attrs,fields


def get_ids(xlsx_path,sheet_name,id_field=1):
    query_pattern = '''
        SELECT
            "_rowid_",
            CAST("Field{id_field}" as integer) as "id"
        FROM
            "{lyr_name}"
        WHERE
            "id" > 0
        GROUP BY
            "id"
        '''
    query_ = query_pattern.format(lyr_name=sheet_name,id_field=id_field)
    ids=get_attrs(xlsx_path,query_)
    return ids


def create_check_query(fields,sheet_name):
    fields_=[]
    for k,v in fields.items():
        fields_.append(f'{v} as "{k}"')
    fields_ = ',\n'.join(fields_)
    qery_pattern='''
        SELECT
            "_rowid_",
            {fields_}
        FROM
            "{lyr_name}"
        WHERE
            CAST("id" AS integer) > 0
        '''
    check_query = qery_pattern.format(
        lyr_name=sheet_name,
        fields_=fields_
        )
    return check_query


def do_check_fields(fields,check_fields,catalog):
    for field in check_fields:
        field_set = set(fields[field])
        log(field,q=True)
        log(field_set,q=True)
        for value in field_set:
            check_result = False
            catalog_field = check_fields[field]
            if value is None:
                check_result = True
            elif value in catalog[catalog_field]['names']:
                check_result = True
            elif value in catalog[catalog_field]['abbr']:
                check_result = True
            if check_result==False:
                return check_result,field,value
    return check_result


def serialization_fields(attrs,fields,target_field,sep=";"):
    new_fields=list(field.strip("_") for field in fields)
    for feature_id, feature in attrs.items():
        values_matrix = []
        target = []
        for field in fields:
            values_matrix.append(feature[field].split(sep))
        # count_items = len(values_matrix[0])
        # print(list(zip(*values_matrix)))
        values_matrix=list(zip(*values_matrix))
        for item in values_matrix:
            target.append(dict(zip(new_fields,item)))
        # print(target)
        json_target = json.dumps(target,ensure_ascii=False,separators=(',', ':'))
        # print(json_target)
        feature[target_field]=json_target
    return attrs


def get_obj(objects,obj_type,obj_id):
    obj=None
    if obj_type!='nep':
        obj=list(
            filter(lambda elem: elem.type==obj_type and elem.id==obj_id,objects)
            )
    return obj


def add_service(input_dict):
    out_dict = input_dict
    for k,v in SERVICE_FIELDS.items():
        out_dict[v['name']]=v['val']
    return out_dict


def handling_except(except_text,mode):
    if mode=='script':
        raise Exception(except_text)
    else:
        result_text = 'Error: ' + except_text
    return result_text


def create_report(les_ids,infra_ids,nep_ids,
        les_ngw_ids,infra_ngw_ids,les_line_ngw_ids,infra_line_ngw_ids,
        nep_ngw_ids,objects,nep_feats,geom_attrs):
    if len(les_ngw_ids)>0:
        report_text='@les'
        report_text+='(#load '
        report_text += str(len(les_ngw_ids))
        filter_attrs = list(
            filter(lambda elem: elem.type =='les' and len(elem.pg[0]['NGW_attrs'])>3,objects)
            )
        report_text += ' #attr ' +str(len(filter_attrs))
        report_text += ' #input_g ' +str(len(les_ids))+')'

    if len(les_line_ngw_ids)>0:
        report_text+='==@l_line'
        report_text+='(#load '
        report_text += str(len(les_line_ngw_ids))+' pts'
        line_set=list(
            filter(lambda elem: elem.type =='les' and len(elem.bind_pts)>0,objects)
            )
        count_lines=len(line_set)
        report_text+=' #objs ' + str(count_lines)
        bind_pts=list(
            filter(lambda elem: elem['type'] =='les' and elem['pt_type']==0,geom_attrs)
            )
        report_text += ' #input_g ' +str(len(bind_pts))+')'

    if len(nep_ngw_ids)>0:
        report_text+='==@nep'
        report_text+='(#load '
        report_text += str(len(nep_ngw_ids))
        filter_attrs = list(
            filter(lambda elem: len(elem['fields'])>4,nep_feats)
            )
        report_text += ' #attr ' +str(len(filter_attrs))
        report_text += ' #input_g ' +str(len(nep_ids))+')'

    if len(infra_ngw_ids)>0:
        report_text+='==@infra'
        report_text+='(#load '
        report_text += str(len(infra_ngw_ids))
        filter_attrs = list(
            filter(lambda elem: elem.type =='infra' and len(elem.pg[0]['NGW_attrs'])>3,objects)
            )
        report_text += ' #attr ' +str(len(filter_attrs))
        report_text += ' #input_g ' +str(len(infra_ids))+')'

    # pprint(geom_attrs)
        
    if len(infra_line_ngw_ids)>0:
        report_text+='==@i_line'
        report_text+='(#load '
        report_text += str(len(infra_line_ngw_ids))+' pts'
        line_set=list(
            filter(lambda elem: elem.type =='infra' and len(elem.bind_pts)>0,objects)
            )
        count_lines=len(line_set)
        report_text+=' #objs ' + str(count_lines)
        bind_pts=list(
            filter(lambda elem: elem['type'] =='infra' and elem['pt_type']==0,geom_attrs)
            )
        report_text += ' input_g ' +str(len(bind_pts))+')'

    return report_text


def process():

    log('load args, set options...',s=10,e=10,ch='*') #
    args = get_args()
    q=True
    log(vars(args),pp=True,q=q) #
    cwd = os.getcwd()

    if args.avral_start:
        except_mode='avral'
        DB_PATH=os.path.join('/avral_add_lesobject_table/avral_add_lesobject_table',"srs6.db")
    else:
        except_mode='script'
        DB_PATH=os.path.join(cwd,"srs6.db")
    ngw_creds = (args.ngw_login,args.ngw_pas)
    ngw_utils.NGW_['url']=args.ngw_url
    ngw_utils.NGW_['creds']=ngw_creds
    SERVICE_FIELDS['serviceUser']['val']=args.ngw_login
    SERVICE_FIELDS['serviceCreatedAt']['val']=NOW
    SERVICE_FIELDS['serviceUpdatedAt']['val']=NOW
    option = gdal.GetThreadLocalConfigOption('OGR_XLSX_HEADERS')
    gdal.SetThreadLocalConfigOption('OGR_XLSX_HEADERS','DISABLE')
    
    geom_attrs=[]
    les_ids=[]
    infra_ids=[]
    nep_ids=[]
    input_attrs = {}
    input_attrs_ = {}
    objects = []
    nep_feats=[]
    
    les_ngw_ids=[]
    infra_ngw_ids=[]
    les_line_ngw_ids=[]
    infra_line_ngw_ids=[]
    nep_ngw_ids=[]
    #-------------------------------------------------------------------
    if args.del_from_fid:
        del_from_fid=args.del_from_fid.split(',')
        if int(del_from_fid[0])>0 and args.ngw_les_id:
            log(f'delete les feature from {del_from_fid[0]} ...',s=10,e=10,ch='*') #
            if args.ngw_les_line_id is None:
                handling_except("Не указан line id",except_mode)
            if args.ngw_nep_id is None:
                handling_except("Не указан nep id",except_mode)
            ngw_utils.clear_ngw_from_id(
                int(del_from_fid[0]),
                args.ngw_les_id,args.ngw_les_line_id,args.ngw_nep_id)
        if int(del_from_fid[1])>0 and args.ngw_infra_id:
            log(f'delete infra feature from {del_from_fid[1]} ...',s=10,e=10,ch='*') #
            if args.ngw_infra_line_id is None:
                handling_except("Не указан infra line id",except_mode)
            ngw_utils.clear_ngw_from_id(
                int(del_from_fid[1]),
                args.ngw_infra_id,args.ngw_infra_line_id)
    # sys.exit()
    #-------------------------------------------------------------------
    log('load geom ids...',s=10,e=10,ch='*') #

    les_ids_,fields = get_ids(args.xlsx,GEOM_SHEET_NAMES['les'])
    if les_ids_!={}:
        les_ids=list(v['id'] for v in les_ids_.values())
        
    infra_ids_,fields = get_ids(args.xlsx,GEOM_SHEET_NAMES['infra'])
    if infra_ids_!={}:
        infra_ids=list(v['id'] for v in infra_ids_.values())
        
    nep_ids_,fields = get_ids(args.xlsx,GEOM_SHEET_NAMES['nep'],id_field=2)
    if nep_ids_!={}:
        nep_ids=list(v['id'] for v in nep_ids_.values())
    
    if len(les_ids)==0 and len(infra_ids)==0:
        report_text=handling_except("Нет объектов для импорта",except_mode)
        return report_text
    if len(les_ids)>0 and not(args.ngw_les_id) and not(args.ngw_les_line_id):
        report_text=handling_except("Не указаны les ngw id",except_mode)
        return report_text
    if len(infra_ids)>0 and not(args.ngw_infra_id) and not(args.ngw_infra_line_id):
        report_text=handling_except("Не указаны infra ngw id",except_mode)
        return report_text
    if len(nep_ids)>0 and not(args.ngw_les_id) and not(args.ngw_nep_id):
        report_text=handling_except("Не указан nep ngw id",except_mode)
        return report_text
    log('les_ids',les_ids,q=q)
    log('infra_ids',infra_ids,q=q)
    log('nep_ids',nep_ids,q=q)
    # sys.exit()
    #-------------------------------------------------------------------
    log('load geom attrs...',s=10,e=10,ch='*') #
    for k,v in GEOM_SHEET_NAMES.items():
        geom_attrs_query = GEOM_QUERY[k].format(lyr_name=v)
        current_geom_attrs,fields=get_attrs(args.xlsx,geom_attrs_query)
        for rowid,geom_attr in current_geom_attrs.items():
            geom_attr['type']=k
            geom_attr['geom_type']='point'
            geom_attr['geom']=(float(geom_attr['lon']),float(geom_attr['lat']))
            geom_attr.pop('rowid')
            geom_attr.pop('lon')
            geom_attr.pop('lat')
            geom_attrs.append(geom_attr)
    log(geom_attrs,pp=True,q=True)
    for item in les_ids:
        current_obj=LesObj()
        current_obj.type='les'
        current_obj.id=item
        current_obj.pts.extend(
            list(p for p in geom_attrs if p['parent_id']==item and p['type']=='les' and str(p['pt_type'])=='1')
            )
        current_obj.bind_pts.extend(
            list(p for p in geom_attrs if p['parent_id']==item and p['type']=='les' and str(p['pt_type'])=='0')
            )
        current_obj.neps_pts.extend(
            list(p for p in geom_attrs if p['parent_id']==item and p['type']=='nep' and str(p['pt_type'])=='1')
            )
            
        objects.append(current_obj)
    for item in infra_ids:
        current_obj=LesObj()
        current_obj.type='infra'
        current_obj.id=item
        current_obj.pts.extend(
            list(p for p in geom_attrs if p['parent_id']==item and p['type']=='infra' and str(p['pt_type'])=='1')
            )
        current_obj.bind_pts.extend(
            list(p for p in geom_attrs if p['parent_id']==item and p['type']=='infra' and str(p['pt_type'])=='0')
            )
        objects.append(current_obj)
    log('объектов',len(objects))
    for obj in objects:
        obj.create_obj_pg()
        obj.create_nep_pg()
        obj.create_ogr_pts_geom()
    # sys.exit()
    #-------------------------------------------------------------------
    log('check sheets...',s=10,e=10,ch='*') #
    for k,v in SHEET_NAMES.items():
        if len(get_ids(args.xlsx,v)[0])>0:
            input_attrs[k]=True
    log('input_attrs:',input_attrs,q=q) #
    # sys.exit()
    #-------------------------------------------------------------------
    if ('les' in input_attrs or 'infra' in input_attrs) and args.check:
        log('load catalog ...',s=10,e=10,ch='*') #
        if args.avral_start:
            path_to_commons = os.path.join('/avral_add_lesobject_table/avral_add_lesobject_table/catalogsList','commons.xml')
        else:
            path_to_commons = os.path.join(cwd,'catalogsList','commons.xml')
        log('path_to_commons',path_to_commons,q=q)
        commons_data, path_to = fileutils.read_afile(path_to_commons)
        commons_dict = rlh_check.commons_xml_to_dict (commons_data)
        commons_dict.update(custom_catalog)
        commons_text = json.dumps(commons_dict,indent = 4,ensure_ascii=False)
        if args.avral_start:
            path_to_commons = os.path.join('/avral_add_lesobject_table/avral_add_lesobject_table/catalogsList','commons.json')
        else:
            path_to_commons = os.path.join(cwd,'catalogsList','commons.json')
        with open(path_to_commons,'w',encoding="utf-8") as fl:
            fl.write(commons_text)
        for poly_type in ('les','infra'):
            if poly_type in input_attrs:
                log(f'check {poly_type} attrs from xlsx...',s=10,e=10,ch='*') #
                check_fields = FIELDS_COMMONS['all'].copy()
                check_fields.update(FIELDS_COMMONS[poly_type])

                check_query = create_check_query(FIELDS[poly_type],SHEET_NAMES[poly_type])
                log(check_query,q=True)
                poly_attrs,poly_fields = get_attrs(args.xlsx,check_query,get_fields=True)
                log(poly_fields,pp=True,q=q)
                
                check_result = do_check_fields(poly_fields,check_fields,commons_dict)
                log('check_result:',check_result)
                if check_result!=True:
                    report_text=handling_except(
                        f"Не соответствие каталогу РЛХ {check_result}",except_mode)
                    return report_text
    # sys.exit()
    #-------------------------------------------------------------------
    for poly_type in SHEET_NAMES:
        if poly_type in input_attrs:
            log(f'load {poly_type} attrs from xlsx...',s=10,e=10,ch='*') #
            poly_query = QUERY[poly_type].format(
                fields=FIELDS[poly_type],
                lyr_name=SHEET_NAMES[poly_type]
                )
            log(poly_query,q=True) #
            poly_attrs,poly_fields = get_attrs(args.xlsx,poly_query)
            if poly_type!='nep':
                poly_attrs = serialization_fields(poly_attrs,['__vd','__area'],"VD")
                poly_attrs = serialization_fields(
                    poly_attrs,
                    ['_vd','_farm','_volume','_poroda','_typeRub','_formaRub'],
                    "PORODA"
                    )
            input_attrs_[poly_type]=poly_attrs
            log(poly_attrs,pp=True,q=q)
    # sys.exit()
    #-------------------------------------------------------------------
    log(f'put attrs to LesObj...',s=10,e=10,ch='*') #
    for obj in objects:
        obj.pg[0]['NGW_attrs']={}
        if obj.type in input_attrs_:
            if obj.id in input_attrs_[obj.type]:
                obj.pg[0]['NGW_attrs']=input_attrs_[obj.type][obj.id]
        obj.pg[0]['NGW_attrs']=add_service(obj.pg[0]['NGW_attrs'])
        log(f'patch {obj.type} {obj.id} to ngw...',s=10,e=10,ch='*') #
        geom=transform(obj.pg[0]['geom'],args.crs,'3857',DB_PATH)
        if geom is None:
            report_text=handling_except(
                f"Ошибка СК или перепроецирования",except_mode)
            return report_text
        poly_feat=[dict(
            fields=obj.pg[0]['NGW_attrs'],
            geom=geom.ExportToWkt()
            )]
        log(poly_feat,pp=True,q=q)
        if obj.type=='les':
            status_code,feat_ngw_id = ngw_utils.patch_to_ngw(args.ngw_les_id,poly_feat)
            if status_code!=200:
                report_text=handling_except(
                    f"Ошибка запроса,{obj.type} ответ сервера {status_code} {feat_ngw_id}",except_mode)
                return report_text
            les_ngw_ids.append(feat_ngw_id[0]['id'])
        elif obj.type=='infra':
            status_code,feat_ngw_id = ngw_utils.patch_to_ngw(args.ngw_infra_id,poly_feat)
            if status_code!=200:
                report_text=handling_except(
                    f"Ошибка запроса,{obj.type} ответ сервера {status_code} {feat_ngw_id}",except_mode)
                return report_text
            infra_ngw_ids.append(feat_ngw_id[0]['id'])
        log('get id',feat_ngw_id,q=False)
        obj.ngw_id=str(feat_ngw_id[0]['id'])
        if len(obj.bind_pts)>0:
            log(f'patch {obj.id} line to ngw...',s=10,e=10,ch='*')
            line_feats=[]
            for p in obj.bind_pts:
                p['NGW_attrs']={}
                p['NGW_attrs'][ID_KEY_FIELD]=obj.ngw_id
                p['NGW_attrs']['idpnt']=p['pt_name']
                p['NGW_attrs']['type']=0
                p['NGW_attrs']=add_service(p['NGW_attrs'])
                geom=transform(p['geom_'],args.crs,'3857',DB_PATH)
                if geom is None:
                    report_text=handling_except(
                        f"Ошибка СК или перепроецирования",except_mode)
                    return report_text
                line_feat=dict(
                    fields=p['NGW_attrs'],
                    geom=geom.ExportToWkt()
                    )
                line_feats.append(line_feat)
            pprint(line_feats)
            if obj.type=='les':
                status_code,line_ngw_ids = ngw_utils.patch_to_ngw(args.ngw_les_line_id,line_feats)
                if status_code!=200:
                    report_text=handling_except(
                        f"Ошибка запроса,{obj.type} line ответ сервера {status_code} {line_ngw_ids}",except_mode)
                    return report_text
                les_line_ngw_ids.extend(line_ngw_ids)
            elif obj.type=='infra':
                status_code,line_ngw_ids = ngw_utils.patch_to_ngw(args.ngw_infra_line_id,line_feats)
                if status_code!=200:
                    report_text=handling_except(
                        f"Ошибка запроса,{obj.type} line ответ сервера {status_code} {line_ngw_ids}",except_mode)
                    return report_text
                infra_line_ngw_ids.extend(line_ngw_ids)
            log('get id',line_ngw_ids,q=False)
        if len(obj.neps_pg)>0:
            for nep in obj.neps_pg:
                log(f'patch {obj.id} nep {nep} to ngw...',s=10,e=10,ch='*')
                obj.neps_pg[nep]['NGW_attrs']={}
                if 'nep' in input_attrs_:
                    if nep in input_attrs_['nep']:
                        obj.neps_pg[nep]['NGW_attrs']=input_attrs_['nep'][nep]
                obj.neps_pg[nep]['NGW_attrs']=add_service(obj.neps_pg[nep]['NGW_attrs'])
                obj.neps_pg[nep]['NGW_attrs'][ID_KEY_FIELD]=obj.ngw_id
                geom=transform(obj.neps_pg[nep]['geom'],args.crs,'3857',DB_PATH)
                if geom is None:
                    report_text=handling_except(
                        f"Ошибка СК или перепроецирования",except_mode)
                    return report_text
                poly_feat=[dict(
                    fields=obj.neps_pg[nep]['NGW_attrs'],
                    geom=geom.ExportToWkt()
                    )]
                nep_feats.append(poly_feat[0])
                log(poly_feat,pp=True,q=q)
                status_code,nep_ngw_id = ngw_utils.patch_to_ngw(args.ngw_nep_id,poly_feat)
                if status_code!=200:
                    report_text=handling_except(
                        f"Ошибка запроса,нэп ответ сервера {status_code} {nep_ngw_id}",except_mode)
                    return report_text
                nep_ngw_ids.append(nep_ngw_id[0]['id'])
                log('get id',nep_ngw_id,q=False)
    # sys.exit()
    #-------------------------------------------------------------------
    gdal.SetThreadLocalConfigOption('OGR_XLSX_HEADERS',option)
    report_text = create_report(les_ids,infra_ids,nep_ids,
        les_ngw_ids,infra_ngw_ids,les_line_ngw_ids,infra_line_ngw_ids,
        nep_ngw_ids,objects,nep_feats,geom_attrs)
    return report_text


print('-'*50,'\n')
print('cwd: ', os.getcwd())
# print(sys.argv)
NOW = datetime.now().replace(microsecond=0).isoformat(sep='T')
print(NOW)
report_text=process()
with open(os.path.join('../','report.txt'), "w",encoding="utf-8") as f:
    f.write(report_text)
print('-'*50,'\n')
print(report_text)
print('-'*50)
