#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET

import sqlite3
import sys

def parse_NS(xml_data):
    NS = {}
    ns_row = xml_data.split('\n')[1]
    ns_list = ns_row.split(" ")[1:]
    root_ns = ns_list[0].split("=")[1].strip('"')
    for ns in ns_list[1:]:
        value = ns.split('=')[1].strip('"')
        key=ns.split('=')[0].split(":")[1]
        NS[key]=value
    return NS,root_ns
    
    
def get_tag_name (name_with_ns):
    name_without_ns=name_with_ns
    tag_list = name_with_ns.split('}')
    if len(tag_list)==2:
        name_without_ns=tag_list[1]
    return name_without_ns
    

def fill_field(field,NS):
    values = {'names':[],'abbr':[]}
    for element in field:
        values['names'].append(element.get('name'))
        abbr = element.find('ctс:abbreviation',NS)
        if not abbr is None:
            values['abbr'].append(abbr.text)
        # break
    return values


def commons_xml_to_dict (xml_data):
    commons_dict = {}
    NS,root_ns = parse_NS(xml_data)
    # print(NS,root_ns)
    catalog = ET.fromstring(xml_data)
    for field in catalog:
        # print(field.tag,len(field))
        field_name = get_tag_name(field.tag)
        commons_dict[field_name]=fill_field(field,NS)
        # break
    return commons_dict


#-----------------------------------------------------------------------
def insert_list(table_name,rows,cur,con):
    query_pattern='''
        INSERT INTO '{table_name}' {columns}
        VALUES {values}
        '''
    for row in rows:
        keys = tuple(row.keys())
        values = tuple(row.values())
        query_text = query_pattern.format(
            table_name=table_name,
            columns=keys,
            values=values
            )
        # print(query_text)
        cur.execute(query_text)
    con.commit()
    print(query_text)
    return
    

def delete_query(table_name,cur,con):
    query_text =f'DELETE FROM {table_name}'
    cur.execute(query_text)
    con.commit()
    return
    
    
def get_forestry_catalog (xml_data):
    con = sqlite3.connect(
    "/home/nikita/add_lesobject_layer/add_lesobject_layer/catalogsList/forestry.db")
    cur = con.cursor()
    
    dacha_catalog = {}
    tract_list = []
    subforestry_list = []
    forestry_list = []
    subject_list = []
    land_list = []
    NS,root_ns = parse_NS(xml_data)
    root_ns = '{'+root_ns+'}'
    catalog = ET.fromstring(xml_data)
    tracts = catalog.find(f'{root_ns}tract')
    for tract in tracts:
        tract_name = tract.get('name')
        tract_id = tract.get('id')
        dacha_catalog[tract_id]=tract_name
        subforestry = tract.find('ctс:subforestry',NS)
        subforestry_id = subforestry.get('id')
        tract_list.append({
            'tr_id':tract_id,
            'tr_name':tract_name,
            'sf_id':subforestry_id
            })
    print(len(dacha_catalog))
    print(len(tract_list))
    delete_query('tr_',cur,con)
    insert_list('tr_',tract_list,cur,con)
    
    subforestrys = catalog.find(f'{root_ns}subforestry')
    for subforestry in subforestrys:
        subforestry_name = subforestry.get('name')
        subforestry_id = subforestry.get('id')

        forestry = subforestry.find('ctс:forestry',NS)
        forestry_id = forestry.get('id')
        subforestry_list.append({
            'sf_id':subforestry_id,
            'sf_name':subforestry_name,
            'f_id':forestry_id
            })
    delete_query('sf_',cur,con)
    insert_list('sf_',subforestry_list,cur,con)
    
    forestrys = catalog.find(f'{root_ns}forestry')
    for forestry in forestrys:
        forestry_name = forestry.get('name')
        forestry_id = forestry.get('id')

        subject = forestry.find('ctс:subject',NS)
        subject_id = subject.get('id')
        land = forestry.find('ctс:landType',NS)
        land_id = land.get('id')
        forestry_list.append({
            'f_id': forestry_id,
            'f_name': forestry_name,
            'sub_id': subject_id,
            'land_id': land_id
            })
    delete_query('f_',cur,con)
    insert_list('f_',forestry_list,cur,con)
    
    subjects = catalog.find(f'{root_ns}subject')
    for subject in subjects:
        subject_name = subject.get('name')
        subject_id = subject.get('id')
        subject_list.append({
            'sub_id': subject_id,
            'sub_name': subject_name
            })
    delete_query('sub_',cur,con)
    insert_list('sub_',subject_list,cur,con)
    
    lands = catalog.find(f'{root_ns}landType')
    for land in lands:
        land_name = land.get('name')
        land_id = land.get('id')
        land_list.append({
            'land_id': land_id,
            'land_name': land_name
            })
    delete_query('land_',cur,con)
    insert_list('land_',land_list,cur,con)
    
    
    con.close()
    return
