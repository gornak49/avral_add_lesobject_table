## Testing

patch children resources in https://lesbox.nextgis.com/resource/2 (or user url)

open https://toolbox.nextgis.com/operation/add_lesobject_table

### Test1

* xlsx upload 'заполненный шаблон_лесосека, нэп, инфраструктура.xlsx'
* ngw_url https://lesbox.nextgis.com
* ngw_login user
* ngw_pas forest
* ngw_les_id 10
* ngw_les_line_id 11
* ngw_nep_id 8
* ngw_infra_id 6
* ngw_infra_line_id 7
* del_from_fid 15,5

### Test2

* xlsx upload 'карьер.xlsx'
* ngw_url https://lesbox.nextgis.com
* ngw_login user
* ngw_pas forest
* 'мск 69 зона 2'
* ngw_les_id 10
* ngw_les_line_id 11

### result

* new features in ngw_url/resourse/ngw_id with script datetime in serviceCreatedAt field

* not empty 'report' in string output
