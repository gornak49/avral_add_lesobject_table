import os

from setuptools import setup

requires = [
    'avral',
    'argparse',
    'gdal'
]

setup(
    name='avral_add_lesobject_table',
    version='0.0.1',
    description='Patch object to NextGIS Les',
    classifiers=[
        "Programming Language :: Python",
    ],
    author='nextgis',
    author_email='info@nextgis.com',
    url='http://nextgis.com',
    keywords='add_lesobject_table',
    packages=['avral_add_lesobject_table'],
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'avral_operations': [
            'add_lesobject_table = avral_add_lesobject_table.operations:AddLesobjectTable',
        ],
    }
)
